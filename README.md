# Laboratorio V

##Trabajo practico N°2: Observador observable

###Documentar el ciclo de ejecución de los metodos del patrón utilizado
La clase observable debe extender de la clase Observable de Java, para agregar un observer,
en la clase observable, se debe llamar al metodo "addObserver" y pasar por parametro la clase observer.
Al hacer esto Java agregara al observer a una lista donde se guardan todos los observadores que estan suscriptos a esa clase.

Para notificar un cambio, en la clase observer se debe llamar al metodo "setChanged" esto establecera un atributo booleano de la clase en TRUE,
luego se debe llamar al metodo "notifyObservers" (opcionalmente se le puede pasar un objeto que es el "mensaje" a noticar). Al hacer esto, Java
recorrera la lista de observers que tiene la clase y llamara a su metodo "update".

Las clases observer deben implementar la interface Observer de Java, y redefinir el metodo "update". Este metodo
es al que se llamara cada vez que un observable notifique un cambio.


###¿Qué argumentos se pasan al metodo update y en que momento se ejecuta dicho metodo?
El metodo "update" recibe como primer parametro la clase que notificó el cambio y como segundo parametro el mensaje.
Este metodo se ejecuta cuando la clase observable llama al metodo "notifyObservers".