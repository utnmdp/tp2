package tp2;

import java.util.Observable;

public class Auto extends Observable implements Cloneable{

    private int nivelAgua = 0;
    private int nivelAceite = 0;
    private double presionNeumaticos = 0;
    private String marca = "Desconocido";
    private String modelo = "Desconocido";
    private String patente = "Desconocido";

    public Auto () {
        //
    }

    public Auto (String marca, String modelo, String patente, int nivelAgua, int nivelAceite, double presionNeumaticos) {
        this.marca = marca;
        this.modelo = modelo;
        this.patente = patente;
        this.nivelAgua = nivelAgua;
        this.nivelAceite = nivelAceite;
        this.presionNeumaticos = presionNeumaticos;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public void setNivelAgua(int nivelAgua) {
        try {
            Auto cloned = this.clone();

            this.nivelAgua = nivelAgua;

            this.setChanged();
            this.notifyObservers(cloned);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    public void setNivelAceite(int nivelAceite) {
        try {
            Auto cloned = this.clone();

            this.nivelAceite = nivelAceite;

            this.setChanged();
            this.notifyObservers(cloned);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    public void setPresionNeumaticos(double presionNeumaticos) {
        try {
            Auto cloned = this.clone();

            this.presionNeumaticos = presionNeumaticos;

            this.setChanged();
            this.notifyObservers(cloned);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public String getPatente() {
        return patente;
    }

    public int getNivelAgua() {
        return nivelAgua;
    }

    public int getNivelAceite() {
        return nivelAceite;
    }

    public double getPresionNeumaticos() {
        return presionNeumaticos;
    }

    /**
     * Simula la conduccion del auto
     * @param metros - Distancia en metros a recorrer con el auto
     */
    public void runrun(int metros) {
        for (int i = 0; i < metros; i++) {
            // Cada 10 metros los niveles de agua, aceite y presion de neumaticos cambian por el desgaste (re malo el auto)
            if (i % 10 == 0) {
                // Nivel de desgaste random
                double random = (Math.random() * 50);

                this.setNivelAgua(this.nivelAgua - (int)random);

                this.setNivelAceite(this.nivelAceite - (int)random);

                this.setPresionNeumaticos(this.presionNeumaticos - random);
            }
        }
    }

    @Override
    public Auto clone() throws CloneNotSupportedException {
        Auto cloned = (Auto) super.clone();
        return cloned;
    }
}
