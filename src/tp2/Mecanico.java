package tp2;

import java.util.Observable;
import java.util.Observer;

public class Mecanico implements Observer {

    private String nombre;
    private Auto auto;

    public Mecanico() {
        this.nombre = "Desconocido";
    }

    public Mecanico(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void update(Observable observable, Object args) {
        // Odio java.. java < PHP
        Auto auto = (Auto) observable;
        Auto oldAuto = (Auto) args;

        // Verifica que el nivel de aceite sea el "correcto"
        if (auto.getNivelAceite() < 20) {
            int nuevoNivel = (int)(Math.random() * 100) + 1;
            this.auto.setNivelAceite(nuevoNivel);

            System.out.printf("El nivel de aceite bajo a %s asique le agregue mas para que quede en %s \n", auto.getNivelAceite(), nuevoNivel);
        }

        // Verifica que el nivel de agua sea el "correcto"
        if (auto.getNivelAgua() < 50) {
            int nuevoNivel = (int)(Math.random() * 100) + 1;
            this.auto.setNivelAgua(nuevoNivel);

            System.out.printf("El nivel de agua bajo a %s asique le agregue mas para que quede en %s \n", auto.getNivelAgua(), nuevoNivel);
        }

        // Verifica que la presion de neumaticos sea la "correcta"
        if (auto.getPresionNeumaticos() < 5.0) {
            double nuevoNivel = (Math.random() * 100) + 1;
            this.auto.setPresionNeumaticos(nuevoNivel);

            System.out.printf("La presion de neumaticos bajo a %s asique los deje en %s \n", auto.getPresionNeumaticos(), nuevoNivel);
        }
    }

    /**
     * Metodo que simula el analisis que hace el mecanico para un auto
     * @param auto - Auto a analizar
     */
    public void analizarVehiculo(Auto auto) {
        this.auto = auto;
        this.auto.addObserver(this);

        // Para poder analizar el auto lo usa andando cierta cantidad de metros y haciendo ajustes luego
        this.auto.runrun(100);
    }
}
